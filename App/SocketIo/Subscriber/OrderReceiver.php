<?php

namespace App\SocketIo\Subscriber;

use App\Service\CustomerManager;
use App\Service\OrderManager;
use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventSubscriberInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;
use SfCod\SocketIoBundle\Service\Broadcast;

class OrderReceiver extends AbstractEvent implements EventInterface, EventSubscriberInterface
{
    /**
     * @var Broadcast
     */
    private $broadcaster;
    /**
     * @var CustomerManager
     */
    private $orderManager;

    public function __construct(OrderManager $orderManager, Broadcast $broadcast)
    {
        $this->orderManager = $orderManager;
        $this->broadcaster = $broadcast;
    }

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'send_order';
    }

    /**
     * Emit client event
     * @return array
     */
    public function handle()
    {
        try {
            $order = $this->orderManager->addOrder(
                $this->customerId,
                $this->address,
                $this->products,
                $this->payload['slotId'] ?? null,
                $this->payload['courierId'] ?? null,
                $this->payload['showRoomId'] ?? null

            );
            $response = ['error' => false, 'orderId' => $order->getId(), 'customerId' => $this->customerId];
        } catch (\Throwable $t) {
            $response = ['error' => true, 'customerId' => $this->customerId];
        }
        $this->broadcaster->emit('send_order_response', $response);
    }
}
