<?php

namespace App\SocketIo\Subscriber;

use App\Service\CustomerManager;
use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventSubscriberInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;
use SfCod\SocketIoBundle\Service\Broadcast;

class CustomerRegisterReceiver extends AbstractEvent implements EventInterface, EventSubscriberInterface
{
    /**
     * @var Broadcast
     */
    private $broadcaster;
    /**
     * @var CustomerManager
     */
    private $customerManager;

    public function __construct(CustomerManager $customerManager, Broadcast $broadcast)
    {
        $this->customerManager = $customerManager;
        $this->broadcaster = $broadcast;
    }

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'save_customer';
    }

    /**
     * Emit client event
     * @return array
     */
    public function handle()
    {
        $customer = $this->customerManager->saveCustomer($this->name, $this->email);
        $this->broadcaster->emit('customer_info', ['customerId' => $customer->getId(), 'name' => $customer->getName(), 'email' => $customer->getEmail(), 'sessId' => $this->sessId]);
    }
}
