<?php

namespace App\SocketIo\Subscriber;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventSubscriberInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;

use App\Service\CourierManager;
use SfCod\SocketIoBundle\Service\Broadcast;

class CourierLocationReceiver extends AbstractEvent implements EventInterface, EventSubscriberInterface
{

    private $courierManager;

    private $broadcaster;

    public function __construct(CourierManager $courierManager,Broadcast $broadcast)
    {
        $this->courierManager = $courierManager;
        $this->broadcaster = $broadcast;
    }

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'courier_location';
    }

    /**
     * Emit client event
     * @return array
     */
    public function handle()
    {
        //$data = $this->payload;
        $courierCanDeliver = true;//$this->courierManager->checkAvailability($data);

        if($courierCanDeliver) {
            $this->broadcaster->emit('order_confirmed', ['courierId' => 1]);
        }
    }
}
