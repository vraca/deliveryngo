<?php

namespace App\SocketIo\Subscriber;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventSubscriberInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;

class AddCourierReciver extends AbstractEvent implements EventInterface, EventSubscriberInterface
{
    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'save_courier';
    }

    /**
     * Emit client event
     * @return array
     */
    public function handle()
    {
        $this->container->get(Broadcast::class)->emit('add_courier', ['some key' => 'some value']);
    }
}
