<?php

namespace App\SocketIo\Publisher;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventPublisherInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;

class AddCourierPublisher extends AbstractEvent implements EventInterface, EventPublisherInterface
{
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    public static function name(): string
    {
        return 'save_courier';
    }

    public function fire(): array
    {
        return $this->payload;
    }
}
