<?php

namespace App\SocketIo\Publisher;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventPublisherInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;
use SfCod\SocketIoBundle\Events\EventRoomInterface;

class CustomerRegisterPublisher extends AbstractEvent implements EventInterface, EventPublisherInterface, EventRoomInterface
{
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    public static function name(): string
    {
        return 'customer_register_response';
    }

    public function room(): string
    {
        return 'customer_id_' . $this->sessId;
    }

    public function fire(): array
    {
        return $this->payload;
    }
}
