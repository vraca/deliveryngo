<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180908224112 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courier ADD vehicle_type_id INT NOT NULL');
        $this->addSql('ALTER TABLE courier ADD CONSTRAINT FK_CF134C7CDA3FD1FC FOREIGN KEY (vehicle_type_id) REFERENCES delivery_box (id)');
        $this->addSql('CREATE INDEX IDX_CF134C7CDA3FD1FC ON courier (vehicle_type_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE courier DROP FOREIGN KEY FK_CF134C7CDA3FD1FC');
        $this->addSql('DROP INDEX IDX_CF134C7CDA3FD1FC ON courier');
        $this->addSql('ALTER TABLE courier DROP vehicle_type_id');
    }
}
