<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180908153109 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery_box ADD outer_length INT NOT NULL, ADD outer_width INT NOT NULL, ADD outer_depth INT NOT NULL, ADD inner_length INT NOT NULL, ADD inner_width INT NOT NULL, ADD inner_depth INT NOT NULL, ADD empty_weight INT NOT NULL, DROP length, DROP width, DROP height');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery_box ADD length INT NOT NULL, ADD width INT NOT NULL, ADD height INT NOT NULL, DROP outer_length, DROP outer_width, DROP outer_depth, DROP inner_length, DROP inner_width, DROP inner_depth, DROP empty_weight');
    }
}
