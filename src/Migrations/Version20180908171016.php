<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180908171016 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE slot (id INT AUTO_INCREMENT NOT NULL, courier_id INT NOT NULL, box_id INT NOT NULL, created DATETIME NOT NULL, INDEX IDX_AC0E2067E3D8151C (courier_id), INDEX IDX_AC0E2067D8177B3F (box_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E2067E3D8151C FOREIGN KEY (courier_id) REFERENCES courier (id)');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E2067D8177B3F FOREIGN KEY (box_id) REFERENCES delivery_box (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE slot');
    }
}
