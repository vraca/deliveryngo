<?php

namespace App\Repository;

use App\Entity\DeliveryBox;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DeliveryBox|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeliveryBox|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeliveryBox[]    findAll()
 * @method DeliveryBox[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeliveryBoxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DeliveryBox::class);
    }

//    /**
//     * @return DeliveryBox[] Returns an array of DeliveryBox objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeliveryBox
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
