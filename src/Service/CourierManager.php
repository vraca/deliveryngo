<?php

namespace App\Service;

use App\Entity\Address;
use App\Entity\Courier;
use App\Entity\DeliveryBox;
use App\Entity\Product;
use App\Entity\Showroom;
use Doctrine\Orm\EntityManagerInterface;
use App\Service\Delivery;
use DVDoug\BoxPacker\ItemList;

class CourierManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Delivery
     */
    private $delivery;

    public function __construct(EntityManagerInterface $entityManager,Delivery $delivery)
    {
        $this->entityManager = $entityManager;
        $this->delivery = $delivery;
    }

    public function addCourier(array $data)
    {
        $deliveryBox = $this->entityManager
            ->getRepository(DeliveryBox::class)->find($data['vehicleType']);
        $courier = new Courier();
        $courier->setName($data['name'])
            ->setSurname($data['surname'])
            ->setPhone($data['phone'])
            ->setGender($data['gender'])
            ->setEmail($data['email'])
            ->setVehicleType($deliveryBox);

        $this->entityManager->persist($courier);
        $this->entityManager->flush();

        return $courier->getId();
    }

    public function getCourierId(string $courierEmail)
    {
        $courierRepository = $this->entityManager->getRepository(Courier::class);
        $courier = $courierRepository->findBy(array('email' => $courierEmail));
        if(empty($courier)) return 0;

        return reset($courier)->getId() ?? 0;
    }

    public function checkAvailability(array $location, array $orderData)
    {
        $address = new Address();
        $address->setLatitude($location['lat'])
                ->setLongitude($location['long']);


        $products = new ItemList();
        foreach ($orderData as $product) {
//            try {
                $product = $this->entityManager->getRepository(Product::class)->find($product);
//            }catch (\Throwable $t){
//                file_put_contents('/var/www/log.data', $t->getMessage(),FILE_APPEND);
//            }

            if (!empty($product)) {
                $products->insert($product);
            }
        }

        $slot = $this->delivery->checkDelivery($address, $products);

        return $slot;
    }

    public function checkCourierLocation(array $orderData)
    {
        $address = new Address();
        $address->setLatitude($orderData['lat'])
                ->setLongitude($orderData['lng']);

        $showroom = $this->entityManager->getRepository(Showroom::class)->find($orderData['data']['order']['showroom']);
        $courier = $this->entityManager->getRepository(Courier::class)->find(1);//$orderData['courier']);

        $products = new ItemList();

        foreach ($orderData['data']['order']['products'] as $product) {
            $product = $this->entityManager->getRepository(Product::class)->find($product);
            if (!empty($product)) {
                $products->insert($product);
            }
        }

        return $this->delivery->checkCourierAvailability($address,$showroom,$courier,$products);

    }
}