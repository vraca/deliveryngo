<?php

namespace App\Service;


use App\Entity\Address;
use App\Entity\Courier;
use App\Entity\DeliveryBox;
use App\Entity\Showroom;
use App\Entity\Slot;
use App\Exception\ProductsDoNotFitException;
use App\Exception\TooFarFromShowroomException;
use App\Repository\DeliveryBoxRepository;
use App\Repository\ShowroomRepository;
use DVDoug\BoxPacker\ItemList;
use DVDoug\BoxPacker\PackedBox;
use Doctrine\ORM\EntityManagerInterface;

class Delivery
{
    const MAX_DISTANCE = 5000;
    const MAX_COURIER_DISTANCE = 8000;
    /**
     * @var ShowroomRepository
     */
    private $showroomRepository;
    /**
     * @var Volumetry
     */
    private $volumetry;
    /**
     * @var DeliveryBoxRepository
     */
    private $deliveryBoxRepository;

    public function __construct(EntityManagerInterface $entityManager,
        Volumetry $volumetry
    ) {
        $this->deliveryBoxRepository = $entityManager->getRepository(DeliveryBox::class);
        $this->showroomRepository = $entityManager->getRepository(Showroom::class);
        $this->volumetry = $volumetry;
    }

    public function checkDelivery(Address $address, ItemList $products): ?Slot
    {
        $closestShowroom = $this->getClosestShowroom($address);
        if (null === $closestShowroom) {
            throw new TooFarFromShowroomException();
        }
        $availableBoxes = $this->getFittedDeliveryBoxes($products);
        if (count($availableBoxes) === 0) {
            throw new ProductsDoNotFitException();
        }
        $slot = $this->checkForSlots($closestShowroom, $products);
        return $slot;
    }

    public function checkCourierAvailability(Address $address, Showroom $showroom, Courier $courier, ItemList $products)
    {
        $distance = $this->computeDistance($address->getLatitude(),$address->getLongitude(),$showroom->getLatitude(),$showroom->getLongitude());

        $availableBoxes = $this->getFittedDeliveryBoxes($products);

        return $distance <= self::MAX_COURIER_DISTANCE && array_key_exists($courier->getVehicleType()->getId(),$availableBoxes);
    }

    public function getClosestShowroom(Address $address): ?Showroom
    {
        $showrooms = $this->showroomRepository->findAll();

        $closestShowroom = null;
        foreach ($showrooms as $showroom) {
            if ($this->canDeliveryFromShowroom($address, $showroom)) {
                $closestShowroom = $showroom;
            }
        }
        return $closestShowroom;
    }

    public function getFittedDeliveryBoxes(ItemList $products)
    {
        $allBoxes = $this->deliveryBoxRepository->findAll();

        $availableBoxes = [];
        foreach ($allBoxes as $box) {
            try {
                $this->volumetry->putItemsInBox($products, $box);
                $availableBoxes[$box->getId()] = $box;
            } catch (ProductsDoNotFitException $e) {
            }
        }

        return $availableBoxes;
    }

    private function canDeliveryFromShowroom(Address $address, Showroom $showroom)
    {
        $computedDistance = $this->computeDistance(
            $showroom->getLatitude(),
            $showroom->getLongitude(),
            $address->getLatitude(),
            $address->getLongitude()
        );
        return $computedDistance < self::MAX_DISTANCE;
    }

    private function checkForSlots(Showroom $showroom, ItemList $products): ?Slot
    {
        $foundSlot = null;
        foreach ($showroom->getSlots() as $slot) {
            try {
                $this->productsFitInSlots($slot, $products);
                $foundSlot = $slot;
            } catch (\Throwable $e) {
                error_log($e);
            }
        }

        return $foundSlot;
    }

    /**
     * @throws \App\Exception\ProductsDoNotFitException
     */
    public function productsFitInSlots(Slot $slot, ItemList $products): ?PackedBox
    {
        foreach ($slot->getOrders() as $order) {
            $products->insertFromArray($order->getProducts()->toArray());
        }
        return $this->volumetry->putItemsInBox($products, $slot->getBox());
    }

    public function computeDistance(
        float $latitudeFrom,
        float $longitudeFrom,
        float $latitudeTo,
        float $longitudeTo,
        int $earthRadius = 6371000
    ): float {
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
}