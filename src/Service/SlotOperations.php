<?php
/**
 * Created by PhpStorm.
 * User: florinbabes
 * Date: 08.09.2018
 * Time: 20:10
 */

namespace App\Service;


use App\Entity\Courier;
use App\Entity\DeliveryBox;
use App\Entity\Showroom;
use App\Entity\Slot;
use App\Repository\SlotRepository;
use Doctrine\ORM\EntityManagerInterface;
use DVDoug\BoxPacker\ItemList;
use DVDoug\BoxPacker\PackedBox;


class SlotOperations
{
    /**
     * @var SlotRepository
     */
    private $slotRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var Volumetry
     */
    private $volumetry;

    public function __construct(
        SlotRepository $slotRepository,
        EntityManagerInterface $entityManager,
        Volumetry $volumetry
    ) {
        $this->slotRepository = $slotRepository;
        $this->entityManager = $entityManager;
        $this->volumetry = $volumetry;
    }

    public function openSlot(Courier $courier, DeliveryBox $box, Showroom $showroom)
    {
        $slot = new Slot();
        $slot->setBox($box);
        $slot->setCourier($courier);
        $slot->setCreated(new \DateTime());
        $slot->setShowroom($showroom);
        $this->entityManager->persist($slot);
        $this->entityManager->flush();
    }
}