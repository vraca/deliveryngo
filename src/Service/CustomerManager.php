<?php
/**
 * Created by PhpStorm.
 * User: florinbabes
 * Date: 09.09.2018
 * Time: 01:48
 */

namespace App\Service;


use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;

class CustomerManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function saveCustomer(string $name, string $email): Customer
    {
        $customer = new Customer();
        $customer->setName($name)
            ->setEmail($email);
        $this->entityManager->persist($customer);
        $this->entityManager->flush();
        return $customer;
    }
}