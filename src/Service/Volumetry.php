<?php

namespace App\Service;


use App\Entity\DeliveryBox;
use App\Exception\ProductsDoNotFitException;
use App\Repository\DeliveryBoxRepository;
use App\Repository\ProductRepository;
use DVDoug\BoxPacker\ItemList;
use DVDoug\BoxPacker\ItemTooLargeException;
use DVDoug\BoxPacker\PackedBox;
use DVDoug\BoxPacker\Packer;

class Volumetry
{
    /**
     * @var DeliveryBoxRepository
     */
    private $deliveryBoxRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(DeliveryBoxRepository $deliveryBoxRepository, ProductRepository $productRepository)
    {
        $this->deliveryBoxRepository = $deliveryBoxRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @throws ProductsDoNotFitException
     */
    public function putItemsInBox(ItemList $products, DeliveryBox $box): ?PackedBox
    {
        $packedBoxes = null;
        $packer = new Packer();
        $packer->setMaxBoxesToBalanceWeight(0);
        $packer->addBox($box);
        $packer->setItems($products);
        try {
            $packedBoxes = $packer->pack();
        } catch (ItemTooLargeException $e) {
            throw new ProductsDoNotFitException();
        }
        if ($packedBoxes->count() > 1) {
            throw new ProductsDoNotFitException();
        }
        return null !== $packedBoxes ? $packedBoxes->top() : null;
    }
}
