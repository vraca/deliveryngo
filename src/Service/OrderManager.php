<?php

namespace App\Service;


use App\Entity\Address;
use App\Entity\Courier;
use App\Entity\Customer;
use App\Entity\DeliveryBox;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Showroom;
use App\Entity\Slot;
use Doctrine\ORM\EntityManagerInterface;
use DVDoug\BoxPacker\ItemList;

class OrderManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, Delivery $delivery)
    {
        $this->delivery = $delivery;
        $this->entityManager = $entityManager;
    }

    public function addOrder(
        int $customerId,
        $address,
        $products,
        ?int $slotId,
        ?int $courierId = null,
        ?int $showRoomId = null
    ): Order {
        $customer = $this->getCustomer($customerId);
        $address = $this->getAddress($address);
        $products = $this->buildProducts($products);
        $order = new Order();
        $order->setCustomer($customer);
        $order->setAddress($address);
        $address->setCustomer($customer);

        $this->entityManager->persist($address);
        foreach ($products as $product) {
            $order->addProduct($product);
        }
        if (null !== $slotId && 0 !== $slotId) {
            $slot = $this->getSlot($slotId);
            $this->trySlot($slot, $order);
        } elseif (null !== $courierId) {
            /** @var Courier $courier */
            $courier = $this->entityManager->getRepository(Courier::class)->find($courierId);
            $slot = $this->getOpenSlotForCourier($courier);
            if (null === $slot) {
                $slot = $this->openSlot($courier, $courier->getVehicleType(), $this->getShowroom($showRoomId));
            }
            $this->trySlot($slot, $order);
        }
        return $order;
    }

    private function getShowroom(int $showroomId)
    {
        return $this->entityManager->getRepository(Showroom::class)->find($showroomId);
    }

    public function openSlot(Courier $courier, DeliveryBox $box, Showroom $showroom): Slot
    {
        $slot = new Slot();
        $slot->setBox($box);
        $slot->setCourier($courier);
        $slot->setCreated(new \DateTime());
        $slot->setShowroom($showroom);
        return $slot;
    }

    private function trySlot(Slot $slot, Order $order)
    {
        $productList = new ItemList();
        $productList->insertFromArray($order->getProducts()->toArray());
        $this->delivery->productsFitInSlots($slot, $productList);

        $order->setSlot($slot);
        $this->entityManager->persist($slot);
        $this->entityManager->flush();
        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }

    public function getAddress(array $addressData)
    {
        $address = new Address();
        $address->setAddress($addressData['address']);
        $address->setLatitude($addressData['lat'])->setLongitude($addressData['long']);
        return $address;
    }

    private function getCustomer(int $customerId)
    {
        return $this->entityManager->getRepository(Customer::class)->find($customerId);
    }

    private function buildProducts(array $productIds)
    {
        $repository = $this->entityManager->getRepository(Product::class);
        $products = [];
        foreach ($productIds as $productId) {
            $products[] = $repository->find($productId);
        }
        return $products;
    }

    private function getSlot(int $slotId)
    {
        return $this->entityManager->getRepository(Slot::class)->find($slotId);
    }

    private function getOpenSlotForCourier(Courier $courier): ?Slot
    {
        return $this->entityManager->getRepository(Slot::class)->findOneBy(['courier' => $courier->getId()]);
    }

}