<?php

namespace App\SocketIo\Subscriber;

use App\Entity\Address;
use App\Service\Delivery;
use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventSubscriberInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;

use App\Service\CourierManager;
use SfCod\SocketIoBundle\Service\Broadcast;

class GetAvailableCouriersReceiver extends AbstractEvent implements EventInterface, EventSubscriberInterface
{

    private $courierManager;

    private $broadcaster;

    private $delivery;

    public function __construct(CourierManager $courierManager,Broadcast $broadcast,Delivery $delivery)
    {
        $this->courierManager = $courierManager;
        $this->broadcaster = $broadcast;
        $this->delivery = $delivery;
    }

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'get_available_couriers';
    }

    /**
     * Emit client event
     * @return array
     */
    public function handle()
    {
        $slot = $this->courierManager->checkAvailability($this->payload['address'], $this->payload['products']);

        if(is_null($slot)){
            $address = new Address();
            $address->setLongitude($this->payload['address']['long'])
                ->setLatitude($this->payload['address']['lat']);

            $closestShowroom = $this->delivery->getClosestShowroom($address);

            $this->payload['showroom'] = $closestShowroom->getId();

            $this->broadcaster->emit('get_location', ['order' => $this->payload]);
        }
    }
}
