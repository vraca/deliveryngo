<?php

namespace App\SocketIo\Subscriber;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventSubscriberInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;

use App\Service\CourierManager;
use SfCod\SocketIoBundle\Service\Broadcast;

class AddCourierReceiver extends AbstractEvent implements EventInterface, EventSubscriberInterface
{

    private $courierManager;

    private $broadcaster;

    public function __construct(CourierManager $courierManager,Broadcast $broadcast)
    {
        $this->courierManager = $courierManager;
        $this->broadcaster = $broadcast;
    }

    /**
     * Changel name. For client side this is nsp.
     */
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    /**
     * Event name
     */
    public static function name(): string
    {
        return 'save_courier';
    }

    /**
     * Emit client event
     * @return array
     */
    public function handle()
    {
        try {
            $data = $this->payload;
            $courierId = $this->courierManager->addCourier($data);

            $this->broadcaster->emit('add_courier', ['courierId' => $courierId, 'sessId' => $this->sessId]);
        }catch (\Throwable $t){
            file_put_contents('/var/www/log.data', $t->getMessage(),FILE_APPEND);
        }
    }
}
