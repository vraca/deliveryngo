<?php

namespace App\SocketIo\Publisher;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventPublisherInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;
use SfCod\SocketIoBundle\Events\EventRoomInterface;

class OrderAddPublisher extends AbstractEvent implements EventInterface, EventPublisherInterface, EventRoomInterface
{
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    public static function name(): string
    {
        return 'send_order_response';
    }

    public function room(): string
    {
        return 'customer_id_' . $this->customerId;
    }

    public function fire(): array
    {
        return $this->payload;
    }
}
