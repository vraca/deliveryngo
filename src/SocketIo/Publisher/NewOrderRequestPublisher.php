<?php

namespace App\SocketIo\Publisher;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventPublisherInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;
use SfCod\SocketIoBundle\Events\EventRoomInterface;
class NewOrderRequestPublisher extends AbstractEvent implements EventInterface, EventPublisherInterface, EventRoomInterface
{
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    public static function name(): string
    {
        return 'new_order_request';
    }

    public function room(): string
    {
        return 'order_requests_' . $this->courierId;
    }

    public function fire(): array
    {
        return $this->payload;
    }
}
