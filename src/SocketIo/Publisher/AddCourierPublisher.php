<?php

namespace App\SocketIo\Publisher;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventPublisherInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;
use SfCod\SocketIoBundle\Events\EventRoomInterface;

class AddCourierPublisher extends AbstractEvent implements EventInterface, EventPublisherInterface, EventRoomInterface
{
    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    public static function name(): string
    {
        return 'add_courier';
    }

    public function fire(): array
    {
        return $this->payload;
    }

    /**
     * Get room name
     *
     * @return string
     */
    public function room(): string
    {
        return 'add_courier_sess_' . $this->sessId;
    }
}
