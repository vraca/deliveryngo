<?php

namespace App\SocketIo\Publisher;

use SfCod\SocketIoBundle\Events\EventInterface;
use SfCod\SocketIoBundle\Events\EventPublisherInterface;
use SfCod\SocketIoBundle\Events\AbstractEvent;

use App\Service\CourierManager;
use SfCod\SocketIoBundle\Events\EventRoomInterface;

class CourierLoginPublisher extends AbstractEvent implements EventInterface, EventPublisherInterface, EventRoomInterface
{
    private $courierManager;

    public function __construct(CourierManager $courierManager)
    {
        $this->courierManager = $courierManager;
    }

    public static function broadcastOn(): array
    {
        return ['notifications'];
    }

    public static function name(): string
    {
        return 'courier_login_response';
    }

    public function room(): string
    {
        return 'courier_id_' . $this->sesId;
    }

    public function fire(): array
    {
        return $this->payload;
    }
}
