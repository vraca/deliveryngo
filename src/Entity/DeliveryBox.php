<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use DVDoug\BoxPacker\Box;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeliveryBoxRepository")
 */
class DeliveryBox implements Box
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $outerLength;

    /**
     * @ORM\Column(type="integer")
     */
    private $outerWidth;

    /**
     * @ORM\Column(type="integer")
     */
    private $outerDepth;

    /**
     * @ORM\Column(type="integer")
     */
    private $innerLength;

    /**
     * @ORM\Column(type="integer")
     */
    private $innerWidth;

    /**
     * @ORM\Column(type="integer")
     */
    private $innerDepth;
    /**
     * @ORM\Column(type="integer")
     */
    private $maxWeight;
    /**
     * @ORM\Column(type="integer")
     */
    private $emptyWeight;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Courier", mappedBy="vehicleType")
     */
    private $couriers;

    public function __construct()
    {
        $this->couriers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getInnerLength(): int
    {
        return $this->innerLength;
    }

    /**
     * @param mixed $innerLength
     * @return DeliveryBox
     */
    public function setInnerLength($innerLength)
    {
        $this->innerLength = $innerLength;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInnerWidth(): int
    {
        return $this->innerWidth;
    }

    /**
     * @param mixed $innerWidth
     * @return DeliveryBox
     */
    public function setInnerWidth($innerWidth)
    {
        $this->innerWidth = $innerWidth;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInnerDepth(): int
    {
        return $this->innerDepth;
    }

    /**
     * @param mixed $innerDepth
     * @return DeliveryBox
     */
    public function setInnerDepth($innerDepth)
    {
        $this->innerDepth = $innerDepth;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmptyWeight(): int
    {
        return $this->emptyWeight;
    }

    /**
     * @param mixed $emptyWeight
     * @return DeliveryBox
     */
    public function setEmptyWeight($emptyWeight)
    {
        $this->emptyWeight = $emptyWeight;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOuterLength(): int
    {
        return $this->outerLength;
    }

    public function setOuterLength(int $outerLength): self
    {
        $this->outerLength = $outerLength;

        return $this;
    }

    public function getOuterWidth(): int
    {
        return $this->outerWidth;
    }

    public function setOuterWidth(int $outerWidth): self
    {
        $this->outerWidth = $outerWidth;

        return $this;
    }

    public function getOuterDepth(): int
    {
        return $this->outerDepth;
    }

    public function setOuterDepth(int $outerDepth): self
    {
        $this->outerDepth = $outerDepth;

        return $this;
    }

    public function getMaxWeight(): int
    {
        return $this->maxWeight;
    }

    public function setMaxWeight(int $maxWeight): self
    {
        $this->maxWeight = $maxWeight;

        return $this;
    }

    /**
     * Reference for box type (e.g. SKU or description).
     *
     * @return string
     */
    public function getReference(): string
    {
        return $this->id;
    }

    /**
     * @return Collection|Courier[]
     */
    public function getCouriers(): Collection
    {
        return $this->couriers;
    }

    public function addCourier(Courier $courier): self
    {
        if (!$this->couriers->contains($courier)) {
            $this->couriers[] = $courier;
            $courier->setVehicleType($this);
        }

        return $this;
    }

    public function removeCourier(Courier $courier): self
    {
        if ($this->couriers->contains($courier)) {
            $this->couriers->removeElement($courier);
            // set the owning side to null (unless already changed)
            if ($courier->getVehicleType() === $this) {
                $courier->setVehicleType(null);
            }
        }

        return $this;
    }
}
